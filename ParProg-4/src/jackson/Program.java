/**
 * 
 */
package jackson;

import java.util.Scanner;

/**
 * Skriv om programmet så att det använder <em>funktioner</em> istället.
 * 
 * <strike>Det finns en buggar i programmet, fixa det.</strike>
 * 
 * Det finns åtminstone plats för att använda fyra funktioner 
 * för att göra programmet enklare att läsa.  Skapa och använd dessa
 * funktioner så att programmet blir lättare att läsa. 
 * 
 * Kom ihåg att använda argument till funktionerna om ni vill kunna
 * använda funktionen med olika värden. Exempelvis
 * <code>
 * myPrintFunction(myStringVariable);
 * </code>
 * 
 * Ni får gärna göra flera förbättringar när ni är klara med att
 * skriva funktionerna.
 * 
 * @author <a href="mailto:anders.jackson@hig.se?subject='Program java class'">Anders Jackson</a>
 * @version 2019-10-02
 */
public class Program {

	/**
	 * Huvudprogrammet
	 * 
	 * Skriv om det så att det använder funktioner med lämpliga
	 * namn så att det blir lättare att använda.
	 * 
	 * @param args används ej
	 */
	public static void main(String[] args) {
		System.out.println("Mata in flyttal tills du skriver in Stop eller Avsluta");
		System.out.println("För varje tal skriver du ut den accumulativa summan");
		System.out.println("samt det så långt minsta och största talet");
		Scanner in = new Scanner(System.in);
		double summa=0;
		double största=0;
		double minsta=0;
		boolean första = true;
		System.out.print("Mata in ett tal: ");
		boolean avsluta = false;
		while (! avsluta) {
			while ( ! avsluta && ! in.hasNextDouble()) {
				String s = in.next();

				if ("Stop".equals(s.trim()) || "Avsluta".equals(s.trim())) {
					System.out.println("Avslutar");
				} else {
					System.out.println("Fel, mata in ett nytt tal");
					System.out.print("Mata in ett tal: ");
				}
			}
			if (! avsluta) {
				double tal = in.nextDouble();
				summa += tal;
				if (tal < minsta || första)
					minsta = tal;
				if (tal > största || första)
					största = tal;
				första = false;
				System.out.printf("Summa %f, Min %f Max %f%n", summa, minsta, största);
				System.out.print("Mata in ett tal (eller Stop eller Avsluta): ");
			}
		}
		System.out.printf("Summa %f, Min %f Max %f%n", summa, minsta, största);

		System.out.println("Tack för att du använt programmet");
		in.close();
		System.exit(0);
	}

}
